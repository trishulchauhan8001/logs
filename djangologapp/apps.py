from django.apps import AppConfig


class DjangologappConfig(AppConfig):
    name = 'djangologapp'
