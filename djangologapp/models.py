from django.db import models
from datetime import datetime
from auditlog.registry import auditlog


class TestStudent1(models.Model):
    name = models.CharField(max_length=50)


class TestTeacher1(models.Model):
    name = models.CharField(max_length=50)


auditlog.register(TestStudent1)
auditlog.register(TestTeacher1)
